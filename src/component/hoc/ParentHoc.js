import React from 'react'
import withCounter from './withCounter'
import AddCounter from './AddCounter'
import SubstractCounter from './SubstractCounter'



const ParentHoc = (props) => {
   
  return (
    <div>
      <AddCounter addBy={4}/>
      <SubstractCounter operator="sub" subBy={10}/> 
    </div>
  )
}

export default withCounter(ParentHoc)











































