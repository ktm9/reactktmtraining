import React from 'react'
import withCounter from './withCounter'

const AddCounter = (props) => {
  return (
    <div>
         count:{props.count+props.addBy}
          <div> <button onClick={props.increment}> increment</button></div>
       
    </div>
  )
}

export default withCounter(AddCounter)