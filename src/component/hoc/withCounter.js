import React from 'react'

const withCounter = (OldComponent) => {
 return(props)=>{
    const{addBy}=props
    console.log(props)
    const [count,setCount]=React.useState(100)
    const handleIncrement=()=>{
        setCount(count+addBy)
    }
    const handleDecrement=()=>{
       switch (props.operator) {
        case "sub":
            setCount(count-props.subBy)
            break;
            case "multiply":
            setCount(count*props.subBy)
            break;
       
        default:
            setCount(count+props.subBy);
       }
    }
   
    return(
        <OldComponent {...props}
         count={count} 
        increment={handleIncrement} 
        decreament={handleDecrement}  />

    )
   
 }
 // return NewComponent // anonymous function banauna
}

export default withCounter