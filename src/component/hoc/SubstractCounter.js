import React from 'react'
import withCounter from './withCounter'

const SubstractCounter = (props) => {
  return (
    <div>
         count:{props.count}
        <div><button onClick={props.decreament} > decreament</button></div>
    </div>
  )
}

export default withCounter(SubstractCounter)