import React from 'react'

function Hooks() {
  const [isTrue,setIStrue]=React.useState(false)
  const [counter,setCounter]=React.useState(0)
  const handleClick=()=>{
    setIStrue(!true)
  }
  console.log('first')
  React.useEffect(()=>{
    console.log('success')
    setCounter(counter+1)
    setIStrue(true)

  },[isTrue])
  console.log('outside')
  return (
    <div onClick={handleClick}>
    {counter}
    click me</div>
  )
}

export default Hooks
