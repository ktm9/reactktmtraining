import { Route, createBrowserRouter, createRoutesFromElements } from "react-router-dom";
import ArrayData from "./ArrayData";
import App from "../App.js";
import { lazy } from "react";
const Filter=lazy(()=>import("./Filter"));
const Hooks=lazy(()=>import("./Hooks.js"))
const UserDashBoard = lazy(() => import("./UserDashBoard"))
const DashboardLayout = lazy(() => import('../layout/DashboardLayout'));
const CallbackParent = lazy(()=>import('./callback/CallbackParent.js'))
const DataStructure=lazy(()=>import('./destructuring/DataStructuring.js'))
const Hoc=lazy(()=>import('./hoc/ParentHoc.js'))
const HocDecreament=lazy(()=>import('./hoc/ParentHoc1.js'))



export const Routers = createBrowserRouter(
  createRoutesFromElements(
    <Route>
      <Route path="/" element={<App />} />
      <Route path="/ArrayData" element={<ArrayData />} />
      <Route path="/filter" element={<Filter />} />
      <Route path="/hooks" element={<DashboardLayout />}>
        <Route index element={<Hooks />} />
        <Route path="user" element={<UserDashBoard />} />
      </Route>
      <Route path="/callback" element={<CallbackParent/>} />
      <Route path="/datastructuring" element={<DataStructure/>} />
      <Route path="/hoc" element={<Hoc/>} />
      <Route path="hoc/decreamet" element={<HocDecreament/>} />
    
     
    </Route>
  )
);
