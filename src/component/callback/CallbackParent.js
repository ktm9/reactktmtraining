import React from "react"
import CallbackChild from "./CallbackChild"

const CallbackParent=()=>{
    const [ChildMessage,setChildMessage]= React.useState({
        name:"prashant",
        address:"ktm"
    })
    console.log("parent",ChildMessage)

    return(
        <div>
            childback parent
            <div>Name:{ChildMessage.name}</div>
            <div>Adress:{ChildMessage.address}</div>
            <CallbackChild 
            Child={ChildMessage} 
            setChildMessage={(e)=>setChildMessage(e)}/>

        </div>
    )
}
export default CallbackParent