const CallbackChild =(props)=>{

    console.log(props.child)
    const handleMessage=()=>{
        const message="hello world!"
        props.setChildMessage(message)
        props.setChildMessage({
            name:'jems',
            address:'bkt'
        })
    }
    return(
        <div>

            {/* {props.ChildMessage} */}
           <div> this is callback</div>
           <div>
            <button onClick={handleMessage}>Button</button>
           </div>

        </div>
    )
}
export default CallbackChild