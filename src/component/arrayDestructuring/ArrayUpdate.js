import React from 'react'

const ArrayUpdate = () => {
  const [changeArray,setchangeArray] =React.useState(arrayData)
  React.useEffect(()=>{
    // setchangeArray([(arrayData[0]="hello"),...changeArray])
    const finalArray=[...changeArray]
    finalArray[0]="hello"
    console.log("new",finalArray)
    setchangeArray(finalArray)
  },[])
  console.log("changeArray",changeArray)
  return (
    <div>Array update</div>
    
  )
}

export default ArrayUpdate
const arrayData=["hari","ram","gita","shyam"]