import React from 'react'
import ArrayUpdate from './ArrayUpdate'


const ArrayDestructuring = () => {
    const arrayData=["hari","shyam","gita","fulmaya"]
    const arrayData1=["pokhara","bhaktapur","lalitpur","kathmandu"]
    arrayData.push("nepal")
    arrayData.pop();
    arrayData[1]="India"
    const mergeArrayData=[...arrayData1,...arrayData]
    console.log("mergeArrayData",mergeArrayData)

    console.log("arrayData",arrayData1)
    const [first,second,...rest]=arrayData
    console.log("first",first)
    console.log("second",second)
    console.log("rest",rest)
  return (
    <div>ArrayDestructuring</div>
   
  )
}

export default ArrayDestructuring