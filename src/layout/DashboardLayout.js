import React from 'react'
import { Outlet } from 'react-router-dom'

function DashboardLayout() {
  return (
    <div>
      this is a dashboard layout
      <div>
        <Outlet/>
    
      </div>
    </div>
  )
}

export default DashboardLayout
