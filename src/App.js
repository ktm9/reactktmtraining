import React from 'react'
import { Link } from 'react-router-dom'

 function App() {
    const links=[
        {
          name:"Array Data",
          link:'/ArrayData'
        },
        // {
        //   name:"Filter",
        //   link:'/filter'
        // },
        {
          name:"Hooks",
          link:'/hooks/'
        },
        {
          name:"user Dashboard",
          link:'hooks/user'
        },
        {
          name:"Call Back",
          link:'/callback'
        }
        ,
        {
          name:"Datastructuring",
          link:'/datastructuring'
        }
        ,
        {
          name:"HOC",
          link:'/hoc'
        },
        {
          name:"hoc decreamet",
          link:'hoc/decreamet'
        }
      
      ]
  return (
    <div className='maincontainer'>
      {
        links.map((item)=>(
            <div key={item.link} className='keyname'>
              <Link to ={`${item.link}`} className='linkname'>
              {item.name}
              </Link>
            </div>
    
          ))
      }
    </div>
  )
}

export default App
